<?php
/*
Plugin Name: Real Response Coupon Checker
Description: A plugin to get the list of coupon usage for a specific coupon
Author: Lucas Williams
Version: 0.1
*/
add_action('admin_menu', 'test_plugin_setup_menu');

function test_plugin_setup_menu(){
	add_menu_page( 'Booking List', 'Booking List', 'manage_options', 'real-coupon-usage', 'coupon_list' );
	add_menu_page('Bulk Coupons', 'Bulk Coupons', 'manage_options', 'real-coupon-bulk', 'bulk_coupons');
	add_menu_page('Booking Links', 'Booking Links', 'manage_options', 'real-booking-link', 'generate_links');
    add_menu_page('Course Discounts', 'Course Discounts', 'manage_options', 'real-discounts', 'discount_codes');
    add_menu_page('Coupon Usage', 'Coupon Usage', 'manage_options', 'real-timeframe', 'coupon_timeframe');
    add_menu_page('Xero Settings', 'Xero Settings', 'manage_options', 'xero-settings', 'xero_settings');
}

function coupon_list(){
	global $wpdb;
	$base = implode(DIRECTORY_SEPARATOR, explode('/', $_SERVER['DOCUMENT_ROOT']));

    $config = $base.DIRECTORY_SEPARATOR.'booking'.DIRECTORY_SEPARATOR.'config.php';
    require_once($config);

    $xero = $base.DIRECTORY_SEPARATOR.'booking'.DIRECTORY_SEPARATOR.'xero.class.php';
    require_once($xero);
    
    if(isset($_GET['booking'])) {
        echo "<h1>Booking Details</h1>";
        echo '<div class="welcome-panel">';
        $sql = "SELECT b.booking_content, b.coupon, b.price, bl.postdata, b.po_number
                FROM wp_rr_bookings b
                JOIN wp_rr_booking_log bl ON bl.booking_id = b.id
                WHERE b.id = ".$_GET['booking'];
        $details = $wpdb->get_results($sql);
        echo $details[0]->booking_content;
        if(strlen($details[0]->coupon) > 0) {
            echo '<p><strong>Coupon: </strong>'.$details[0]->coupon.'</p>';
        }
    
        if(strlen($details[0]->price) > 0) {
            echo '<p><strong>Price: </strong>'.$details[0]->price.'</p>';
        }
        echo '</div>';

        $postdata = print_r_reverse($details[0]->postdata);
        //echo '<pre>'; var_dump($postdata); echo '</pre>';
        $clicker = $postdata['clicks'];
        //echo '<pre>'; var_dump($clicker); echo '</pre>';
        $clicks = json_decode(stripslashes($clicker));
        //echo '<pre>'; var_dump($clicks); echo '</pre>';

        echo '<div class="welcome-panel">';
        echo '<h2>POST data</h2>';
        echo '<ul>';
        foreach($postdata as $key => $value) {
            if($key != 'clicks') {
                echo '<li><strong>' . $key . '</strong> ' . $value . '</li>';
            }
        }
        echo '</ul>';
        echo '</div>';

        echo '<div class="welcome-panel">';

        echo '<h2>Clicks</h2>';
        echo '<table class="wp-list-table widefat fixed striped pages"><tr><th>Button Type</th><th>Button Text</th><th>Button Data</th></tr>';
        foreach($clicks as $click) {
            //echo '<pre>'; var_dump($click); echo '</pre>';
            $classes = [];
            if(is_object($click->classes)) {
                $classlist = $click->classes;
            } else {
                $classlist = [];
                $clickparts = explode(';', $click->classes);
                foreach($clickparts as $part) {
                    $parts = explode(' ', $part);
                    foreach($parts as $part2) {
                        $classlist[] = $part2;
                    }
                }
            }
            //echo '<pre>'; var_dump($classlist); echo '</pre>';
            foreach($classlist as $key => $class) {
                //if(!in_array($class, ['btn', 'btn-primary', 'btn-default', 'btn-success', 'pull-right'])) {
                if(strlen($class) > 0) {
                    $classes[] = $class;
                }
            }

            if(count($classes) > 0 || strlen($click->text) > 0) {
                $classText = str_replace([':,', 'ID:', ', Classes:'], [':', '<strong>ID:</strong>', '<br><strong>Classes:</strong>'], implode(', ', $classes));
                echo '<tr><td>'.$classText.'</td>';

                echo '<td>' . $click->text . '</td>';

                echo '<td>';
                echo '<ul>';
                foreach ($click->data as $key => $value) {
                    echo '<li><strong>' . $key . '</strong> ' . $value . '</li>';
                }
                echo '</ul>';
                echo '</td></tr>';
            }
        }
        echo '</table>';
        echo '</div><!- .welcome-panel -->';

        echo '<div class="welcome-panel">';
        echo '<h2>Log Data</h2>';
        echo '<pre>';
        $sql = "SELECT * FROM wp_rr_booking_log WHERE booking_id = ".$_GET['booking'];
        $details = $wpdb->get_results($sql);
        foreach($details as $detail) {
            var_dump($details);
        }
        echo '</pre>';
        echo '</div><!- .welcome-panel -->';

        echo '</div>';
    } else {
        $pagenum = $_GET['p'] ?? 0;

        if(isset($_GET['show_invoice'])) {
            $invoice_id = $_GET['show_invoice'];
            $xeroObject = new XeroFunctions($xeroclientid, $xerosecret, 'Xero', 'User', 'Updating Invoices', date('Y-m-d'));
            $pdf = $xeroObject->getXeroInvoicePDF($invoice_id);
            //echo '<pre>'; var_dump($pdf); echo '</pre>';
            $path = $pdf->getPathName();
            //echo $path.'<br>';

            //var_dump(file_exists($path));
            $move = copy($path, 'I:\\work\\realresponse\\pdf\\fpc-'.date('Y-m-d-H-i-s').'.pdf');
            //var_dump($move);
        }

        if(isset($_GET['retry_axcontact'])) {
            $sql = "SELECT booking_content FROM wp_rr_bookings WHERE id = ".$_GET['retry_axcontact'];
            $details = $wpdb->get_results($sql);

            $input = $details[0]->booking_content;

            $pos1 = strpos($input, '<li>Participants');
            $str1 = substr($input, $pos1);
            $pos2 = strpos($str1, '</ul>');
            $str2 = substr($str1, 0, $pos2);

            $parts = explode('<li>', $str2);
            unset($parts[0]);
            unset($parts[1]);

            $i = 0;
            $participants = [];
            foreach($parts as $part) {
                $idx = (int)($i / 3);
                $part = trim(str_replace('</li>', '', $part));
                switch($i % 3) {
                    case 0 :
                        //name
                        $nameparts = explode(' ', $part, 2);
                        $participants[$idx]['first'] = $nameparts[0];
                        $participants[$idx]['last'] = $nameparts[1];
                        break;

                    case 1:
                        //Email
                        $participants[$idx]['email'] = str_replace('Email: ', '', $part);
                        break;

                    case 2:
                        //Phone
                        $participants[$idx]['phone'] = str_replace('Phone: ', '', $part);
                        break;
                }

                $i++;
            }

            foreach($participants as $part) {
                $ax_url = 'https://realresponse.app.axcelerate.com.au/api/';
                $proxy = false;
                if($_SERVER['HTTP_HOST'] == 'realresponse.local' || $_SERVER['HTTP_HOST'] == 'dev.realresponse.com.au') {
                    $ax_url = 'https://stg.axcelerate.com/api/';
                }

                if($_SERVER['HTTP_HOST'] == 'realresponse.local') {
                    $proxy = true;
                }
                $api_token = '986B5C41-48E8-4D11-A68D0A8228FC3336';
                $ws_token = '00DAD897-4E82-4C3D-92D5B0A0050E7603';

                $service_url = $ax_url.'contacts/search?givenName='.urlencode($part['first']).'&surname='.urlencode($part['last']);

                //echo $service_url;

                $headers = array(
                    'WSToken: ' . $ws_token,
                    'APIToken: ' . $api_token,
                    'Expect: '
                );

                $curl = curl_init($service_url);
                curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                if ($proxy) {
                    curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                    curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                    curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                }

                $curl_response = curl_exec($curl);
                $data = json_decode($curl_response);

                //echo '<pre>'; var_dump($data); echo '</pre>';
                $contact_id = 0;
                foreach ($data as $item) {
                    //verify email or phone
                    $workphone = preg_replace('/[^0-9+]/', '', $item->WORKPHONE);
                    $mobilephone = preg_replace('/[^0-9+]/', '', $item->MOBILEPHONE);
                    $homephone = preg_replace('/[^0-9+]/', '', $item->PHONE);
                    if($part['email'] == $item->EMAILADDRESS && ($part['phone'] == $mobilephone || $part['phone'] == $workphone || $part['phone'] == $homephone)) {
                        $fullMatches++;
                        $fullMatchId = $item->CONTACTID;
                    } elseif($part['email'] == $item->EMAILADDRESS || $part['phone'] == $mobilephone || $part['phone'] == $workphone || $part['phone'] == $homephone) {
                        $partialMatches++;
                        $partialMatchId = $item->CONTACTID;
                    }
                }

                if($fullMatches == 1) {
                    $contact_id = $fullMatchId;
                } else {
                    if($partialMatches == 1) {
                        $contact_id = $partialMatchId;
                    }
                }

                if($contact_id == 0) {
                    $service_url = $ax_url . 'contact';

                    $headers = array(
                        'wstoken: ' . $ws_token,
                        'apitoken: ' . $api_token
                    );

                    $params = array(
                        'givenName' => $part['first'],
                        'surname' => $part['last'],
                        'emailAddress' => $part['email'],
                        'mobilephone' => $part['phone']
                    );

                    $fieldsstring = http_build_query($params);

                    $curl = curl_init($service_url);
                    curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
                    curl_setopt($curl, CURLOPT_POST, true);
                    curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
                    if ($proxy) {
                        curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                        curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                        curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
                    }

                    $curl_response = curl_exec($curl);
                    $data = json_decode($curl_response);
                    $contact_id = $data->CONTACTID;
                }

                $sql = "UPDATE wp_rr_bookings SET axcel_contact_id = ".$contact_id." WHERE id = ".$_GET['retry_axcontact'];
                $wpdb->query($sql);
            }
        }

        if(isset($_GET['retry_enrolment'])) {
            $sql = 'SELECT axcel_contact_id, axcel_instance_id, price, participants FROM wp_rr_bookings WHERE id = '.$_GET['retry_enrolment'];
            $details = $wpdb->get_results($sql);

            $contact_id = $details[0]->axcel_contact_id;
            $instance = $details[0]->axcel_instance_id;
            $pricePer = floatval(str_replace('$', '', $details[0]->price)) / $details[0]->participants;

            $service_url = 'https://realresponse.app.axcelerate.com.au/api/course/enrol';
            if($_SERVER['HTTP_HOST'] == 'realresponse.local' || $_SERVER['HTTP_HOST'] == 'dev.realresponse.com.au') {
                $service_url = 'https://stg.axcelerate.com/api/course/enrol';
            }
            $api_token = '986B5C41-48E8-4D11-A68D0A8228FC3336';
            $ws_token = '00DAD897-4E82-4C3D-92D5B0A0050E7603';

            $headers = array(
                'wstoken: ' . $ws_token,
                'apitoken: ' . $api_token
            );

            $params = array(
                'contactId' => $contact_id,
                'instanceId' => $instance,
                'type' => 'w',
                'generateInvoice' => true,
                'cost' => $pricePer,
                'discountIDList' => '1528',
                'suppressNotifications' => 'true'
            );

            if(false) {
                $params['forceBooking'] = 'true';
            }

            $fieldsstring = http_build_query($params);

            $curl = curl_init($service_url);
            curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
            curl_setopt($curl, CURLOPT_POST, true);
            curl_setopt($curl, CURLOPT_POSTFIELDS, $fieldsstring);
            /*if ($proxy) {
                curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
                curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
                curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
            }*/

            $curl_response = curl_exec($curl);
            $data = json_decode($curl_response);
            echo '<pre>'; var_dump($data); echo '</pre>'; die();
        }

        if(isset($_GET['retry_payment']) || isset($_GET['retry_invoice']) || isset($_GET['retry_contact'])) {
            //echo $_GET['retry_payment'].'<br>';
            $id = (isset($_GET['retry_payment']) ? $_GET['retry_payment'] : (isset($_GET['retry_invoice']) ? $_GET['retry_invoice'] : $_GET['retry_contact']));
            
            //echo $id.'<br>';
            $sessionid = uniqid();
            $sql = "SELECT *
				FROM wp_rr_bookings b
				WHERE b.id = ".$id;
            //echo $sql;
            $detail = $wpdb->get_results($sql);
            $details = $detail[0];

            $wpdb->insert('wp_rr_booking_log', [
                'id' => $sessionid,
                'booking_id' => $id
            ]);
            
            //var_dump($details); echo '<br>';
    
            $terms = get_the_terms($details->course_id, 'rfa_course_types');
            $taxcode = 'EXEMPTOUTPUT';
            $taxrate = 0;
    
            $divisor = 1.0;
            $type = 'Public';
            if($type != 'Onsite') {
                $account_code = get_field('account_code_public', 5059);
                $bank_code = get_field('bank_account_code_public', 5059);
                if(is_array($terms) && count($terms) > 0) {
                    $term_id = $terms[0]->term_id;
                    $new_account_code = get_field('xero_account_code', 'rfa_course_types_' . $term_id);
                    if (strlen($new_account_code) > 0) {
                        $account_code = $new_account_code;
                    }
            
                    $gst_status = get_field('gst_status', 'rfa_course_types_' . $term_id);
                    if ($gst_status == 1) {
                        $taxcode = 'OUTPUT';
                        $taxrate = 10;
                        $divisor = 1.1;
                    }
                }
            } else {
                $account_code = get_field('account_code_onsite', 5059);
                $bank_code = get_field('bank_account_code_onsite', 5059);
            }
    
            $amount = str_replace('$', '', $details->price);

            $xeroObject = new XeroFunctions($xeroclientid, $xerosecret, $details->first_name, $details->last_name, $details->course_name, $details->course_date);
            if (isset($_GET['retry_payment']) || isset($_GET['retry_invoice']) || isset($_GET['retry_contact'])) {
                $xerosuccess = $xeroObject->getOrCreateXeroContact($details->xero_customer_id, $details->inv_org, $details->first_name, $details->last_name, $details->email, $id, $sessionid);
                //var_dump($xerosuccess);
            }
            
            if(isset($_GET['retry_payment']) || isset($_GET['retry_invoice'])) {
                $xerosuccess = $xeroObject->getOrCreateXeroInvoice($xerosuccess, $gst_status, $amount, $details->course_id, $account_code, $taxcode, $id, $divisor, $sessionid, $details->po_number);
                //var_dump($xerosuccess);
            }
    
            if (isset($_GET['retry_payment'])) {
                $xerosuccess = $xeroObject->addPaymentToInvoice($xerosuccess, $amount, $details->stripe_payment_id, $id, $bank_code, $sessionid);
                //var_dump($xerosuccess);
            }
        }
        
        $restrictCode = false;
        $code = '';
        if (isset($_GET['code'])) {
            $restrictCode = true;
            $code = $_GET['code'];
        }
        echo "<h1>Booking List</h1>";
        echo '<h2>' . $code . '</h2>';

        $sql = "SELECT b.id, b.booking_contact, b.location_name, b.course_name, b.course_date, b.booking_date, b.coupon, b.price, b.participant_names, b.stripe_customer_id, b.stripe_payment_id, b.xero_contact_id, b.xero_invoice_id, b.xero_payment_id, b.axcel_contact_id, b.axcel_enrolment_id, b.axcel_instance_id, (select bl.postdata from wp_rr_booking_log bl where bl.booking_id = b.id limit 1) as postdata
        FROM wp_rr_bookings b";
        if ($restrictCode) {
            $sql .= "WHERE b.coupon = '" . $_GET['code'] . "'";
        }
        $sql .= " ORDER BY b.id DESC LIMIT 50 OFFSET ".($pagenum * 50);
        //echo $sql;

        $uses = $wpdb->get_results($sql);
        echo '<table class="wp-list-table widefat fixed striped posts">
    <thead>
        <tr>
            <th>Contact</th>
            <th>Course</th>
            <th>Location</th>
            <th>Participants</th>
            <th>Coupon</th>
            <th>Course Date</th>
            <th>Booking Date</th>
            <th style="text-align:right;">Price</th>
            <th style="text-align:center;">Stripe Contact</th>
            <th style="text-align:center;">Stripe Payment</th>
            <th style="text-align:center;">Xero Contact</th>
            <th style="text-align:center;">Xero Invoice</th>
            <th style="text-align:center;">Xero Payment</th>
            <th style="text-align:center;">aXcelerate Contact</th>
            <th style="text-align:center;">aXcelerate Enrolment</th>
            <th style="text-align:center;">Zapier</th>
            <th style="text-align:center;">Invoice</th>
        </tr>
    </thead>
    <tbody>';
        foreach ($uses as $use) {
            echo '<tr>
        <td><a href="admin.php?page=real-coupon-usage&booking=' . $use->id . '">' . $use->booking_contact . '</a></td>
        <td>' . $use->course_name . '</td>
        <td>' . $use->location_name . '</td>
        <td>' . $use->participant_names . '</td>
        <td>' . $use->coupon . '</td>
        <td>' . $use->course_date . '</td>
        <td>' . $use->booking_date . '</td>
        <td style="text-align: right;">' . $use->price . '</td>';
            $postdata = print_r_reverse($use->postdata);
            if ($use->price != '$0') {
                if($postdata['takePayment'] == 0) {
                    //Invoiced
                    echo '<td style="text-align: center;" colspan="2">Invoiced. No payment taken.</td>';
                } else {
                    echo '<td style="text-align: center;">';
                    if (strlen($use->stripe_customer_id) > 0) {
                        echo '<a href="https://dashboard.stripe.com/customers/' . $use->stripe_customer_id . '" class="btn btn-success" target="_blank"><i class="fa fa-check"></i></a>';
                    }
                    echo '</td><td style="text-align: center;">';
                    if (strlen($use->stripe_payment_id) > 0) {
                        echo '<a href="https://dashboard.stripe.com/payments/' . $use->stripe_payment_id . '" class="btn btn-success" target="_blank"><i class="fa fa-check"></i></a>';
                    }
                }
                echo '</td><td style="text-align: center;">';
                if (strlen($use->xero_contact_id) > 0) {
                    echo '<a href="https://go.xero.com/Contacts/View/' . $use->xero_contact_id . '" class="btn btn-success" target="_blank"><i class="fa fa-check"></i></a>';
                } else {
                    echo '<a href="admin.php?page=real-coupon-usage&retry_contact=' . $use->id . '" class="btn btn-danger"><i class="fa fa-refresh"></i></a>';
                }
                echo '</td><td style="text-align: center;">';
                if (strlen($use->xero_invoice_id) > 0) {
                    echo '<a href="https://go.xero.com/AccountsReceivable/View.aspx?InvoiceID=' . $use->xero_invoice_id . '" class="btn btn-success" target="_blank"><i class="fa fa-check"></i></a>';
                    echo '</td><td style="text-align: center">';
                    if (strlen($use->xero_payment_id) > 0) {
                        echo '<a href="https://go.xero.com/Bank/ViewTransaction.aspx?bankTransactionID=' . $use->xero_payment_id . '" class="btn btn-success" target="_blank"><i class="fa fa-check"></i></a>';
                    } else {
                        if(strlen($use->stripe_payment_id) > 0) {
                            echo '<a href="admin.php?page=real-coupon-usage&retry_payment=' . $use->id . '" class="btn btn-danger"><i class="fa fa-refresh"></i></a>';
                        }
                    }
                } else {
                    echo '<a href="admin.php?page=real-coupon-usage&retry_invoice=' . $use->id . '" class="btn btn-danger"><i class="fa fa-refresh"></i></a></td><td>';
                }
                echo '</td>';
            } else {
                echo '<td colspan="5"></td>';
            }

            echo '<td style="text-align: right">';
            if (strlen($use->axcel_contact_id) > 0) {
                echo '<a href="https://realresponse.app.axcelerate.com/management/management2/Contact_View.cfm?ContactID='.$use->axcel_contact_id.'" class="btn btn-success" target="_blank"><i class="fa fa-check"></i></a>';
            } else {
                echo '<a href="admin.php?page=real-coupon-usage&retry_axcontact=' . $use->id . '" class="btn btn-danger"><i class="fa fa-refresh"></i></a>';
            }
            echo '</td>';

            echo '<td style="text-align: right">';
            if (strlen($use->axcel_enrolment_id) > 0) {
                echo '<a href="https://realresponse.app.axcelerate.com/management/management2/ProgramStatus.cfm?PDataID='.$use->axcel_instance_id.'" class="btn btn-success" target="_blank"><i class="fa fa-check"></i></a>';
            } else {
                echo '<a href="admin.php?page=real-coupon-usage&retry_enrolment=' . $use->id . '" class="btn btn-danger"><i class="fa fa-refresh"></i></a>';
            }
            echo '</td>';
            echo '<td>';
            if(strlen($use->zapier_in) > 0) {
                if(strlen($use->zapier_out) > 0) {
                    echo '<a class="btn btn-success"><i class="fa fa-check"></i></a>';
                } else {
                    echo '<a class="btn btn-danger"><i class="fa fa-times"></i></a>';
                }
            }
            echo '</td>';
            if(strlen($use->xero_invoice_id) > 0) {
                echo '<td><a class="btn btn-warning" href="admin.php?page=real-coupon-usage&show_invoice=' . $use->xero_invoice_id . '"><i class="fa fa-files-o"></i></a></td>';
            } else {
                echo '<td></td>';
            }
            echo '</tr>';
        }
        echo '</tbody>
    </table>';
        if($pagenum > 0) {
            echo '<a class="button" href="admin.php?page=real-coupon-usage&p='.($pagenum - 1).'">Prev</a>';
        }
        echo '<a class="button" href="admin.php?page=real-coupon-usage&p='.($pagenum + 1).'">Next</a>';
    }
}

function coupon_timeframe() {
    global $wpdb;
    $sql = "SELECT b.id, b.booking_contact, l.post_title as location_name, c.post_title as course_name, b.course_date, b.booking_date, b.coupon, b.price, b.participant_names
            FROM wp_rr_bookings b
            JOIN wp_posts l on l.ID = b.location_id
            JOIN wp_posts c on c.ID = b.course_id 
            WHERE b.coupon is not null and b.coupon <> ''
            ORDER BY b.id DESC";

    $uses = $wpdb->get_results($sql);
    echo "<div class='wrap'><h1>Coupon Usage</h1>";
    echo '<a href="#" class="page-title-action" style="float: right" onclick="doit(\'xlsx\');">Export</a>';
    echo '<table class="wp-list-table widefat fixed striped posts" id="data-table">
        <thead>
            <tr>
				<th>Coupon</th>
                <th>Contact</th>
                <th>Course</th>
                <th>Location</th>
                <th>Participants</th>
                <th>Course Date</th>
                <th>Booking Date</th>
                <th style="text-align:right;">Original Price</th>
                <th style="text-align:right;">Final Price</th>
            </tr>
        </thead>
        <tbody>';
    foreach ($uses as $use) {
        $price = $use->price;
        $priceparts = explode('</strike>', $price);

        echo '<tr>
			<td>' . $use->coupon . '</td>
            <td><a href="admin.php?page=real-coupon-usage&booking=' . $use->id . '">' . $use->booking_contact . '</a></td>
            <td>' . $use->course_name . '</td>
            <td>' . $use->location_name . '</td>
            <td>' . $use->participant_names . '</td>
            <td>' . $use->course_date . '</td>
            <td>' . $use->booking_date . '</td>
            <td style="text-align: right;">' . str_replace('<strike>', '', $priceparts[0]) . '</td>
            <td style="text-align: right;">' . $priceparts[1] . '</td>
        </tr>';
    }
    echo '</tbody>
        </table>
    </div>';

    echo '<script type="text/javascript" src="//unpkg.com/xlsx/dist/shim.min.js"></script>
    <script type="text/javascript" src="//unpkg.com/xlsx/dist/xlsx.full.min.js"></script>
    <script type="text/javascript" src="//unpkg.com/blob.js@1.0.1/Blob.js"></script>
    <script type="text/javascript" src="//unpkg.com/file-saver@1.3.3/FileSaver.js"></script>
    <script>
        function doit(type, fn, dl) {
            var elt = document.getElementById(\'data-table\');
            var wb = XLSX.utils.table_to_book(elt, {sheet:"Sheet JS"});
            return dl ?
                XLSX.write(wb, {bookType:type, bookSST:true, type: \'base64\'}) :
                XLSX.writeFile(wb, fn || (\'coupon-usage-'.date('Y-m-d').'.\' + (type || \'xlsx\')));
        }
    </script>';
}

function generate_links() {
    global $wpdb;
    echo '<link rel="stylesheet" href="'.get_site_url().'/wp-content/plugins/advanced-custom-fields/assets/css/acf-global.css?ver=5.7.7">';
    echo '<link rel="stylesheet" href="'.get_site_url().'/wp-content/plugins/advanced-custom-fields/assets/css/acf-input.css?ver=5.7.7">';
    echo '<div class="wrap">';
    echo '<h1>Generate Booking Links</h1>';
    
    echo '<form method="POST" id="bulk-form">';
    echo '<div class="postbox acf-fields">';
    
    if(isset($_POST)) {
        $show = false;
        $course = '';
        $location = '';
        $type = '';
        $state = '';
        $instance = '';
        $obj = new \stdClass();
        if(isset($_POST['course']) && $_POST['course'] > 0) {
            $obj->cid = $_POST['course'];
            $show = true;
            $course = '<p><strong>Course:</strong> '.get_the_title( $obj->cid ).'</p>';
        }
    
        if(isset($_POST['location']) && $_POST['location'] > 0) {
            $obj->lid = $_POST['location'];
            $show = true;
            $location = '<p><strong>Location:</strong> '.get_the_title( $obj->lid ).'</p>';
        }
        if(isset($_POST['location']) && $_POST['location'] == -1) {
            $obj->lid = $_POST['location'];
            $show = true;
            $location = '<p><strong>Location:</strong> Any</p>';
        }
    
        if(isset($_POST['type']) && $_POST['type'] > -1) {
            $obj->tid = $_POST['type'];
            $show = true;
            $mytype = '';
            switch($obj->tid) {
                case 0 :
                    $mytype = 'Onsite';
                    break;

                case 1 :
                    $mytype = 'Public';
                    break;

                case 2 :
                    $mytype = 'Online';
                    break;
            }
            $type = '<p><strong>Type:</strong> '.$mytype.'</p>';
        }

        if(isset($_POST['state']) && $_POST['state'] > 0) {
            $obj->sid = $_POST['state'];
            $show = true;
            $stateTerm = get_term_by('term_id',  $obj->sid, 'rfa_states');
            //var_dump($stateTerm);
            $statename = '';
            if($stateTerm) {
                $statename = $stateTerm->name;
            }
            $state = '<p><strong>State:</strong> '.$statename.'</p>';
        }
        if(isset($_POST['state']) && $_POST['state'] == -1) {
            $obj->sid = $_POST['state'];
            $show = true;
            $state = '<p><strong>State:</strong> Any</p>';
        }

        if(isset($_POST['instance']) && strlen($_POST['instance']) > 0) {
            $obj->iid = $_POST['instance'];
            $show = true;
            $instance = '<p><strong>Instance ID:</strong> '.$obj->iid.'</p>';
        }
        
        if($show) {

            $link = get_site_url().'/booking/'.base64_encode(json_encode($obj));
            echo '<div class="acf-field acf-field-number">
                <div class="acf-label">
                    <label for="generated-link">Generated Link</label>
                    '.$course.$location.$type.$state.$instance.'
                </div>
                <div class="acf-input">
                    <input type="text" id="generated-link" readonly value="'.$link.'">
                </div>
            </div>';

            //Save that link into the database.
            $wpdb->insert('wp_rr_links', [
                'course' => $obj->cid,
                'state' => $obj->sid,
                'location' => $obj->lid,
                'type' => $obj->tid,
                'instance' => $obj->iid,
                'url' => $link,
            ]);
        }
    }
    
    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="course">Course</label>
		</div>
		<div class="acf-input">
			<select name="course" id="course">
			    <option value="0">(all)</option>';
    $posts = get_posts(['post_type' => 'rfa_courses', 'numberposts' => -1]);
    foreach($posts as $post) {
        $cq = get_field('cq_first_aid', $post->ID);
        echo '<option value="'.$post->ID.'">'.$post->post_title.($cq ? ' [CQ]' : ' [RR]').'</option>';
    }
    echo '</select>
		</div>
	</div>';

    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="state">State</label>
		</div>
		<div class="acf-input">
			<select name="state" id="state">
			    <option value="-99">(all)</option>
			    <option value="-1">Any</option>';
    $posts = $states = get_terms( array(
        'taxonomy' => 'rfa_states',
        'parent' => 0,
        'hide_empty' => false,
    ) );
    foreach($posts as $post) {
        echo '<option value="'.strtoupper($post->term_id).'">'.$post->name.'</option>';
    }
    echo '</select>
		</div>
	</div>';

    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="location">Location</label>
		</div>
		<div class="acf-input">
			<select name="location" id="location">
			    <option value="-99">(all)</option>
			    <option value="-1">Any</option>';
    $posts = get_posts(['post_type' => 'rfa_locations', 'numberposts' => -1]);
    foreach($posts as $post) {
        $cq = get_option('cq_first_aid', $post->ID);
        echo '<option value="'.$post->ID.'">'.$post->post_title.($cq == 1 ? ' [CQ]' : ' [RR]').'</option>';
    }
    echo '</select>
		</div>
	</div>';
    
    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="type">Type</label>
		</div>
		<div class="acf-input">
			<select name="type" id="type">
			    <option value="-1">(all)</option>
			    <option value="0">Onsite</option>
			    <option value="1">Public</option>
			    <option value="2">Online</option>
            </select>
		</div>
	</div>';

    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="instance">Instance ID</label>
		</div>
		<div class="acf-input">
			<input type="text" id="instance" name="instance">
		</div>
	</div>';
    
    echo '<div id="major-publishing-actions">
	<input name="save" type="submit" class="button button-primary button-large" id="publish" value="Get Link">
</div>';
    echo '</div>';
    echo '</form>';

    //Show saved links
    $links = $wpdb->get_results('select * from wp_rr_links');
    echo '<h2>Previously generated links</h2>';
    echo '<table class="wp-list-table widefat fixed striped posts">';
    echo '<tr>
        <th>Course</th>
        <th>State</th>
        <th>Location</th>
        <th>Type</th>
        <th>Instance</th>
        <th>URL</th>
    </tr>';
    foreach($links as $obj) {
        $course = get_the_title( $obj->course);
        $location = get_the_title( $obj->location );
        $stateTerm = get_term_by('term_id',  $obj->state, 'rfa_states');
        //var_dump($stateTerm);
        $statename = '';
        if($stateTerm) {
            $statename = $stateTerm->name;
        }
        $type = '';
        switch($obj->type) {
            case null :
                $type = '';
                break;

            case '0' :
                $type = 'Onsite';
                break;

            case '1' :
                $type = 'Public';
                break;

            case '2' :
                $type = 'Online';
                break;
        }
        echo '<tr>
            <td>'.$course.'</td>
            <td>'.$statename.'</td>
            <td>'.$location.'</td>
            <td>'.$type.'</td>
            <td>'.$obj->instance.'</td>
            <td><a href="'.$obj->url.'">'.$obj->url.'</a></td>
        </tr>';
    }
    echo '</table>';
    echo '</div>';
}

function bulk_coupons() {
    //is there postdata?
    echo '<link rel="stylesheet" href="'.get_site_url().'/wp-content/plugins/advanced-custom-fields/assets/css/acf-global.css?ver=5.7.7">';
    echo '<link rel="stylesheet" href="'.get_site_url().'/wp-content/plugins/advanced-custom-fields/assets/css/acf-input.css?ver=5.7.7">';
	echo '<div class="wrap">';
	echo '<h1>Bulk Create Coupons</h1>';
	
	if (isset($_POST['num_coupons']) || isset($_FILES['coupon_codes'])) {
        //Set execution time to 1 hour
        ini_set('max_execution_time', 3600);
        $filename = 'coupons_' . date('Y-m-d-h-i-s');
        $uploads = wp_upload_dir();
        $file_path = $uploads['path'] . DIRECTORY_SEPARATOR . $filename . '.csv';
        $handle = fopen($file_path, 'a') or die('Could not open ' . $file_path);
        
        $couponcode = [];
        
        if(strlen($_FILES['coupon_codes']['tmp_name']) > 0) {
            move_uploaded_file($_FILES['coupon_codes']['tmp_name'], $uploads['path'].DIRECTORY_SEPARATOR.'input.csv');
            $h = fopen($uploads['path'].DIRECTORY_SEPARATOR.'input.csv', "r");
            while (($data = fgetcsv($h, 1000, ",")) !== FALSE)
            {
                echo '<pre>'; var_dump($data); echo '</pre>';
                // Read the data from a single line
                $couponcode[] = $data[0];
            }
    
            // Close and delete the file
            fclose($h);
            unlink($uploads['path'].DIRECTORY_SEPARATOR.'input.csv');
        } else {
            for ($c = 0; $c < $_POST['num_coupons']; $c++) {
                $chars = "0123456789ABCDEFGHIJKLMNOPQRSTUVWXYZ";
                $res = "";
                for ($i = 0; $i < 10; $i++) {
                    $res .= $chars[mt_rand(0, strlen($chars) - 1)];
                }
                
                $couponcode[] = $res;
            }
        }
        
       	foreach($couponcode as $res) {
			$post_id = wp_insert_post([
				'post_title' => $res,
				'post_type' => 'rfa_coupons',
				'post_status' => 'publish'
			]);
			wp_set_object_terms($post_id, $_POST['category'], 'rfa_coupon_types');

			update_field('uses', $_POST['uses'], $post_id);
			update_field('course', $_POST['courses'], $post_id);
			update_field('location', $_POST['locations'], $post_id);
			update_field('amount', $_POST['amount'], $post_id);
			update_field('amount_type', $_POST['amount_type'], $post_id);
			update_field('usage_type', $_POST['usage_type'], $post_id);
			update_field('start_date', $_POST['start_date'], $post_id);
			update_field('finish_date', $_POST['finish_date'], $post_id);
			update_field('date_type', $_POST['date_type'], $post_id);
			update_field('tracking', (isset($_POST['tracking']) ? '1' : '0'), $post_id);
			fputcsv($handle, [$res]);
		}
		fclose($handle);

		echo '<div id="message" class="updated notice notice-success is-dismissible"><p>Coupons created. <a href="' . $uploads['url'] . '/' . $filename . '.csv" target="_blank">Download a list of them</a>.</div>';
	}

	echo '<form method="POST" id="bulk-form" enctype="multipart/form-data">';
	echo '<div class="postbox acf-fields">';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="num_coupons">Number to create</label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<input type="number" id="num_coupons" name="num_coupons" step="1">
			</div>
		</div>
		<div class="acf-label">
			<label for="coupon_codes">-or- Select a CSV file</label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<input type="file" id="coupon_codes" name="coupon_codes">
			</div>
		</div>
	</div>';
    
    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="course">Category<span class="required">*</span></label>
		</div>
		<div class="acf-input">
			<select name="category" id="category">
			    <option value="0">(please select)</option>';
                $posts = get_terms(['taxonomy' => 'rfa_coupon_types', 'hide_empty' => false]);
                foreach($posts as $post) {
                    echo '<option value="'.$post->name.'">'.$post->name.'</option>';
                }
            echo '</select>
            <div class="validation-error" id="category_error">Please select a category</div>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="uses">Uses per coupon<span class="required">*</span></label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<input type="number" id="uses" name="uses" step="1">
			</div>
			<div class="validation-error" id="uses_error">Please enter a number of uses per coupon, or 0 for unlimited</div>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="course">Course</label>
		</div>
		<div class="acf-input">
			<ul class="acf-bl list choices-list">';
		$posts = get_posts([
			'post_type' => 'rfa_courses',
			'numberposts' => -1
		]);
		foreach($posts as $post) {
			echo '<li><input type="checkbox" name="courses[]" value="'.$post->ID.'">'.$post->post_title.'</li>';
		}
		echo '</ul>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="location">Location</label>
		</div>
		<div class="acf-input">
				<ul class="acf-bl list choices-list">';
		$posts = get_posts([
			'post_type' => 'rfa_locations',
			'numberposts' => -1
		]);
		foreach($posts as $post) {
			echo '<li><input type="checkbox" name="locations[]" value="'.$post->ID.'">'.$post->post_title.'</li>';
		}
		echo '</ul>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="amount">Amount<span class="required">*</span></label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<input type="number" id="amount" name="amount" step="1">
			</div>
			<div class="validation-error" id="amount_error">Please enter an amount</div>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="amount_type">Amount Type</label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<select id="amount_type" class="" name="amount_type">
					<option value="1" selected="selected" data-i="0">Dollars</option>
					<option value="2">Percent</option>
				</select>
			</div>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="usage_type">Usage Type</label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<select id="usage_type" class="" name="usage_type">
					<option value="1">Any number of participants counts as one use (single use per booking)</option>
					<option value="2" selected="selected">Each individual participant counts as one use (multiple uses per booking)</option>
				</select>
			</div>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="start_date">Start Date</label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<input type="date" id="start_date" name="start_date" step="1">
			</div>
		</div>
	</div>';

	echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="finish_date">Finish Date</label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<input type="date" id="finish_date" name="finish_date" step="1">
			</div>
		</div>
	</div>';
    
    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="date_type">Date Type</label>
		</div>
		<div class="acf-input">
			<div class="acf-input-wrap">
				<select id="date_type" class="" name="date_type">
					<option value="0">Course Date</option>
					<option value="1">Usage Date</option>
				</select>
			</div>
		</div>
	</div>';
    
    echo '<div class="acf-field acf-field-number">
		<div class="acf-label">
			<label for="tracking">Don’t track uses of this coupon as a conversion in google analytics</label>
		</div>
		<div class="acf-input">
			<div class="acf-true-false">
                <input name="tracking" value="0" type="hidden">
                <label>
                    <input type="checkbox" id="tracking" name="tracking" value="1" class="" autocomplete="off">
                </label>
            </div>
		</div>
	</div>';

	echo '<div id="major-publishing-actions">
	<input name="save" type="submit" class="button button-primary button-large" id="publish" value="Create Coupons">
</div>';

	echo '</div>';
	echo '</form>';
	echo '</wrap>';
	?>
        <script type="text/javascript">
            jQuery(document).ready(function($) {
                $('.validation-error').hide();
                $('#bulk-form').submit(function(e) {
                    $('.validation-error').hide();
                    _res = true;
                    
                    if($('#category').val() == '' || $('#category').val() == 0) {
                        $('#category_error').show();
                        _res = false;
                    }

                    if($('#uses').val() == '' || $('#uses').val() < 0) {
                        $('#uses_error').show();
                        _res = false;
                    }

                    if($('#amount').val() == '' || $('#amount').val() < 1) {
                        $('#amount_error').show();
                        _res = false;
                    }
                    
                    if(!_res) {
                        $(document).scrollTop();
                    }
                    
                    return _res;
                });
            });
        </script>
    <?php
}

function discount_codes() {
    global $wpdb;
    if(isset($_GET['action']) && $_GET['action'] == 'axcel') {
        //Load courses from aXcelerate
        $config = ABSPATH.DIRECTORY_SEPARATOR.'booking'.DIRECTORY_SEPARATOR.'config.php';
        include_once($config);
        $service_url = $ax_url . 'course/instance/search';
    
        date_default_timezone_set('Australia/Sydney');
        $post = array(
            'type' => 'w',
            'displayLength' => 100,
            'startDate_min' => $_GET['date'],
            'startDate_max' => '2100-01-01',
            'finishDate_min' => $_GET['date'],
            'finishDate_max' => '2100-01-01',
            'enrolmentOpen' => 1,
            'sortColumn' => 9
        );
    
        $postfields = http_build_query($post);
    
        $headers = array(
            'WSToken: ' . $ws_token,
            'APIToken: ' . $api_token,
            'Expect: ',
            'Content-Length: '.strlen($postfields)
        );
    
        $curl = curl_init();
        curl_setopt($curl, CURLOPT_URL, $service_url);
        curl_setopt($curl, CURLOPT_HTTPHEADER, $headers);
        curl_setopt($curl, CURLOPT_POST, 1);
        curl_setopt($curl, CURLOPT_POSTFIELDS, $postfields);
        curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
        if ($proxy) {
            curl_setopt($curl, CURLOPT_PROXY, '127.0.0.1:8888');
            curl_setopt($curl, CURLOPT_SSL_VERIFYHOST, 0);
            curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, 0);
        }
    
        $curl_response = curl_exec($curl);
        curl_close($curl);
        $data = json_decode($curl_response);
        
        //echo '<pre>'; var_dump($data); echo '</pre>'; die();
        
        foreach($data as $courseitem) {
            $wpdb->query("UPDATE wp_axcel_courses SET price = ".$courseitem->COST." WHERE id = ".$courseitem->INSTANCEID);
            $wpdb->query("INSERT IGNORE INTO wp_axcel_courses(id, code, name, coursedate, location, participants, maxparticipants, price, priority) VALUES (".$courseitem->INSTANCEID.", '".$courseitem->CODE."','".$courseitem->NAME."', '".substr($courseitem->STARTDATE, 0, 10)."', '".$courseitem->LOCATION."', ".$courseitem->PARTICIPANTS.", ".$courseitem->MAXPARTICIPANTS.", ".$courseitem->COST.", 0)");
        }
    }
    
    if(isset($_POST['save'])) {
        //Save changes.
        foreach($_POST['price'] as $id => $price) {
            if(strlen($price) > 0) {
                $wpdb->query("UPDATE wp_axcel_courses SET newprice = $price, availability = ".$_POST['availability'][$id].", priority = ".$_POST['priority'][$id]." WHERE id = $id");
            } else {
                $wpdb->query("UPDATE wp_axcel_courses SET newprice = NULL, availability = ".$_POST['availability'][$id].", priority = ".$_POST['priority'][$id]." WHERE id = $id");
            }
        }
    }
    echo '<h1>Course Discounts</h1>';
    echo '<form method="GET" action="admin.php">';
    echo '<input type="hidden" name="page" value="real-discounts">';
    echo '<input type="hidden" name="action" value="axcel">';
    echo '<p>Load more courses, from a specific date:</p>';
    echo '<p><input type="date" value="'.date('Y-m-d', strtotime('tomorrow')).'" name="date"><button type="submit" class="button">Load</button></p>';
    echo '</form>';
    
    $sql = "SELECT * FROM wp_axcel_courses WHERE coursedate >= NOW() order by coursedate";
    $details = $wpdb->get_results($sql);
    echo '<form method="POST">
        <table class="wp-list-table widefat fixed striped posts" style="max-width: 1500px">
        <thead>
            <tr>
                <th width="60px">Instance</th>
                <th width="100px">Code</th>
                <th>Course</th>
                <th width="100px">Date</th>
                <th>Location</th>
				<th style="text-align:right" width="100px">Price</th>
                <th style="text-align:right" width="100px">New Price</th>
                <th>Priority</th>
                <th>Limit Availability</th>
            </tr>
        </thead>
        <tbody>';
    foreach ($details as $detail) {
        //echo '<tr><td colspan="6">'; var_dump($detail); echo '</td></tr>';
        echo '<tr>
            <td>'.$detail->id.'</td>
            <td>'.$detail->code.'</td>
            <td>'.$detail->name.'</td>
            <td>'.date('d/m/Y', strtotime($detail->coursedate)).'</td>
            <td>'.$detail->location.'</td>
			<td style="text-align:right">$'.number_format($detail->price, 2).'</td>
            <td style="text-align:right"><input type="text" name="price['.$detail->id.']" value="'.($detail->newprice == null ? '' : number_format($detail->newprice, 2)).'" style="width: 90px"></td>
            <td>
                <select name="priority['.$detail->id.']">
                    <option value="0">No</option>
                    <option value="1" '.($detail->priority == 1 ? 'selected' : '').'>Yes</option>    
                </select>
            </td>
            <td>
                <select name="availability['.$detail->id.']">
                    <option value="0">Available everywhere</option>
                    <option value="1" '.($detail->availability == 1 ? 'selected' : '').'>Melbourne/Geelong</option>    
                </select>
            </td>
        </tr>';
    }
    echo '</tbody>
        </table>
        <button name="save" value="save">Save prices</button>
    </form>';
}

function xero_settings() {
     echo '<h1>Xero Configuration</h1>
     <p>The Xero configuration has moved to the Portal.  Please visit the relevant portal to configure Xero.</p>';
}

function print_r_reverse($in) {
    $lines = explode("\n", trim($in));
    if (trim($lines[0]) != 'Array') {
        // bottomed out to something that isn't an array
        return $in;
    } else {
        // this is an array, lets parse it
        if (preg_match("/(\s{5,})\(/", $lines[1], $match)) {
            // this is a tested array/recursive call to this function
            // take a set of spaces off the beginning
            $spaces = $match[1];
            $spaces_length = strlen($spaces);
            $lines_total = count($lines);
            for ($i = 0; $i < $lines_total; $i++) {
                if (substr($lines[$i], 0, $spaces_length) == $spaces) {
                    $lines[$i] = substr($lines[$i], $spaces_length);
                }
            }
        }
        array_shift($lines); // Array
        array_shift($lines); // (
        array_pop($lines); // )
        $in = implode("\n", $lines);
        // make sure we only match stuff with 4 preceding spaces (stuff for this array and not a nested one)
        preg_match_all("/^\s{4}\[(.+?)\] \=\> /m", $in, $matches, PREG_OFFSET_CAPTURE | PREG_SET_ORDER);
        $pos = array();
        $previous_key = '';
        $in_length = strlen($in);
        // store the following in $pos:
        // array with key = key of the parsed array's item
        // value = array(start position in $in, $end position in $in)
        foreach ($matches as $match) {
            $key = $match[1][0];
            $start = $match[0][1] + strlen($match[0][0]);
            $pos[$key] = array($start, $in_length);
            if ($previous_key != '') $pos[$previous_key][1] = $match[0][1] - 1;
            $previous_key = $key;
        }
        $ret = array();
        foreach ($pos as $key => $where) {
            // recursively see if the parsed out value is an array too
            $ret[$key] = print_r_reverse(substr($in, $where[0], $where[1] - $where[0]));
        }
        return $ret;
    }
}

?>